import src

RAW_DATA_PATH = 'data/raw/train.csv'
CLEANED_DATA_PATH = 'data/raw/cleaned_data.csv'
RESULT_DATA_PATH = 'data/raw/result_data.csv'

if __name__ == '__main__':
    src.preprocess_data(RAW_DATA_PATH, CLEANED_DATA_PATH)
    src.generate_features(CLEANED_DATA_PATH, RESULT_DATA_PATH)