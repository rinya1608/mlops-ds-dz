import click
import pandas as pd

@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
def generate_features(input_path: str, output_path: str):
    df = pd.read_csv(input_path)
    df['num_features']=df['features'].apply(len)
    df['num_photos'] = df['photos'].apply(len)
    df["num_description_words"] = df["description"].apply(lambda x: len(str(x).split(" ")))
    df["created"] = pd.to_datetime(df["created"])
    df["created_year"] = df["created"].dt.year
    df["created_month"] = df["created"].dt.month
    df["created_day"] = df["created"].dt.day
    df["created_hour"] = df["created"].dt.hour
    mapper = {
        'low': 0,
        'medium': 1,
        'high': 2
    }
    df['interest_level'] = df['interest_level'].apply(lambda x: mapper[x])
    df.to_csv(output_path, index=False)

if __name__ == '__main__':
    generate_features()