# -*- coding: utf-8 -*-
import click
import pandas as pd
import re

@click.command()
@click.argument('input_path', type=click.Path())
@click.argument('output_path', type=click.Path())
def preprocess_data(input_path: str, output_path: str):
    df = pd.read_csv(input_path, sep=';')
    df['features'] = df['features'].str.replace('[\[\]\']', '').str.split(', ')
    df['photos'] = df['photos'].str.replace('[\[\]\']', '').str.split(', ')
    df['description'] = df['description'].fillna('').apply(lambda s: preprocess_desc(s))
    df["created"] = pd.to_datetime(df["created"])
    df.to_csv(output_path, index=False)



def preprocess_desc(s: str) -> str:
    pattern = re.compile(r'[^A-Za-z]+')
    s = "".join(re.sub(pattern, ' ', s).strip().lower())
    words = re.split('\s+', s)
    return " ".join(words)


if __name__ == '__main__':
    preprocess_data()
