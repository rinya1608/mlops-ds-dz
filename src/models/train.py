import json
import os
from typing import List

import click
import mlflow
import pandas as pd
import joblib as jb
from sklearn.metrics import f1_score, mean_absolute_error, mean_squared_error, accuracy_score
from sklearn.model_selection import train_test_split
from xgboost import XGBClassifier


os.environ["MLFLOW_TRACKING_USERNAME"]="rinya1608"
os.environ["MLFLOW_TRACKING_PASSWORD"]="4800bc030be8cf5426c9382409991d272f61604f"

mlflow.set_tracking_uri("https://dagshub.com/rinya1608/mlops-ds-dz-hub.mlflow")
mlflow.set_experiment('yarullin')


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('output_paths', type=click.Path(), nargs=2)
def train(input_path: str, output_paths: str):
    features_baseline = ['price', 'bathrooms',
                         'bedrooms', "num_photos", "num_features",
                         "num_description_words", "created_hour"]
    target = 'interest_level'
    params = {
        'random_state': 42,
        'n_estimators': 400,
        'max_depth': 5
    }
    mlflow.log_params(params)

    train = pd.read_csv(input_path, sep=',')
    train = train.set_index('listing_id')
    train_target = train[target]
    X_train, X_test, y_train, y_test = train_test_split(train[features_baseline], train_target,
                                                        test_size=0.3, stratify=train_target,
                                                        random_state=42)
    y_train.value_counts(normalize=True)
    y_test.value_counts(normalize=True)
    model = XGBClassifier(params)
    model.fit(X_train, y_train)
    y_predicts = model.predict(X_test)

    jb.dump(model, output_paths[0])

    score = dict(
        accuracy=accuracy_score(y_test, y_predicts),
        f1=f1_score(y_test, y_predicts, average='macro')
    )

    mlflow.log_metrics(score)
    mlflow.log_artifact(output_paths[0])
    mlflow.xgboost.log_model(xgb_model=model,
                             artifact_path=output_paths[0],
                             registered_model_name='xgb model'
                             )

    with open(output_paths[1], 'w') as json_file:
        json.dump(score, json_file, indent=4)


if __name__ == '__main__':
    train()
